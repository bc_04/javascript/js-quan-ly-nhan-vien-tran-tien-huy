let formRules = {
  tknv: {
    type: 1,
    rule: "required|minLength[4]|maxLength[6]|validNumbFormat",
    errorEl: "#tbTKNV",
    errors: {
      required: "Trường này không được phép để trống",
      minLength: "Vui lòng nhập tối thiểu {param} ký tự",
      maxLength: "Vui lòng chỉ nhập tối đa {param} ký tự",
      validNumbFormat: "Vui lòng chỉ được phép nhập số",
    },
  },

  name: {
    type: 1,
    rule: "required|validTextFormat",
    errorEl: "#tbTen",
    errors: {
      required: "Trường này không được phép để trống",
      validTextFormat: "Vui lòng không được phép nhập số",
    },
  },

  email: {
    type: 1,
    rule: "required|validEmail",
    errorEl: "#tbEmail",
    errors: {
      required: "Trường này không được phép để trống",
      validEmail: "Vui lòng nhập đúng format email",
    },
  },

  password: {
    type: 1,
    rule: "required|minLength[6]|maxLength[10]|validPassFormat",
    errorEl: "#tbMatKhau",
    errors: {
      required: "Trường này không được phép để trống",
      minLength: "Vui lòng nhập tối thiểu {param} ký tự",
      maxLength: "Vui lòng chỉ nhập tối đa {param} ký tự",
      validPassFormat:
        "Mật khẩu phải chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt",
    },
  },

  datepicker: {
    type: 1,
    rule: "required|validDateFormat",
    errorEl: "#tbNgay",
    errors: {
      required: "Trường này không được phép để trống",
      validDateFormat: "Vui lòng chọn đúng định dạng mm/dd/yyyy",
    },
  },

  luongCB: {
    type: 1,
    rule: "required|greaterThan[1000000]|lessThan[20000000]",
    errorEl: "#tbLuongCB",
    errors: {
      required: "Trường này không được phép để trống",
      greaterThan: "Vui lòng chỉ nhập giá trị tối thiểu {param}",
      lessThan: "Vui lòng nhập giá trị tối đa {param}",
    },
  },

  chucvu: {
    type: 1,
    rule: "required|inList[1,2,3]",
    errorEl: "#tbChucVu",
    errors: {
      required: "Trường này không được phép để trống",
      inList: "Vui lòng nhập giá trị trong khoảng:{param}",
    },
  },

  gioLam: {
    type: 1,
    rule: "required|greaterThan[80]|lessThan[200]",
    errorEl: "#tbGiolam",
    errors: {
      required: "Trường này không được phép để trống",
      greaterThan: "Số giờ làm phải tối thiểu {param} giờ",
      lessThan: "Số giờ làm không được phép vượt quá {param} giờ",
    },
  },
};

export { formRules };
