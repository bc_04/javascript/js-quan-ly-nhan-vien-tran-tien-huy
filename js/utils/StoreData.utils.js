let saveLocalStorage = (name, data) => {
  localStorage.setItem(name, JSON.stringify(data));
};

let loadLocalStorage = (name) => {
  return JSON.parse(localStorage.getItem(name)) || [];
};

let delLocalStorage = (name) => {
  localStorage.removeItem(name);
};

export { saveLocalStorage, loadLocalStorage, delLocalStorage };
