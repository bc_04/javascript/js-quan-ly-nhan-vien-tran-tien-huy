class RenderUtils {
  constructor() {
    this.target = "";
    this.targetData = "";
  }

  setTargetEl(targetEl) {
    this.target = targetEl;
  }

  getTargetEl() {
    return this.target;
  }

  setTargetData(data) {
    this.targetData = data;
  }

  getTargetData() {
    return this.targetData;
  }

  clearContent() {
    document.querySelectorAll(this.getTargetEl())[0].innerHTML = "";
  }

  createHTMLTempl() {
    let htmlTemplate = "";
    let data = this.getTargetData();
    data.forEach((item) => {
      htmlTemplate += `
            <tr class="employee employee-${item._account}">
                <td>${item._account}</td>
                <td>${item._name}</td>
                <td>${item._email}</td>
                <td>${item._workDay}</td>
                <td>${item.getRoleName()}</td>
                <td>${item.calculateTotalPayment()}</td>
                <td>Nhân viên ${item.rankEmployee()}</td>
                <td class="action">
                    <i class="icon icon-edit fa-solid fa-pen-to-square" 
                    data-id="${
                      item._account
                    }" data-toggle="modal" data-target="#myModal"></i>
                    <i class="icon icon-del fa-solid fa-xmark" 
                    data-id="${item._account}"></i>
                </td>
            </tr>
        `;
    });
    return htmlTemplate;
  }

  bindEvents(_target, event, callback) {
    if (_target instanceof HTMLElement) {
      _target.addEventListener(event, callback);
      return;
    } else {
      document.querySelector(_target).addEventListener(event, callback);
    }
  }

  renderHTML({ editCallBack, deleteCallBack }) {
    this.clearContent();
    document.querySelectorAll(this.getTargetEl())[0].innerHTML =
      this.createHTMLTempl();
    document.querySelectorAll(".icon-edit").forEach((editBtn) => {
      this.bindEvents(editBtn, "click", function () {
        editCallBack(editBtn.dataset.id);
      });
    });
    document.querySelectorAll(".icon-del").forEach((delBtn) => {
      this.bindEvents(delBtn, "click", function () {
        deleteCallBack(delBtn.dataset.id);
      });
    });
  }
}

export { RenderUtils };
