export let findItemByKey = (dataList, key, itemToFind) => {
  try {
    if (dataList === undefined) {
      throw "Data input is invalid, please input an array as dataList";
    }

    if (key === undefined) {
      throw "Data input is invalid, please input the key you want to compare with your data";
    }

    if (itemToFind === undefined) {
      throw "Data input is invalid, please input the data you want to find";
    }

    return dataList.findIndex((item) => item[key] === itemToFind);
  } catch (err) {
    console.warn(`Error: ${err}`);
  }
};
