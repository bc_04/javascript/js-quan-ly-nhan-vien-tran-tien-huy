import { Employee } from "./model/employee.model.js";
import {
  showAllEmployee,
  addEmployee,
  updateEmployee,
  searchByRank,
} from "./controller/Employee_controller.js";

showAllEmployee();
document.getElementById("btnThemNV").addEventListener("click", () => {
  addEmployee();
});

document.getElementById("btnCapNhat").addEventListener("click", (e) => {
  let employeeIdx = e.target.dataset.index;
  if (employeeIdx === undefined) {
    console.log("Nothing to update");
    return;
  }
  updateEmployee(employeeIdx);
});

document.getElementById("btnTimNV").addEventListener("click", () => {
  let searchValue = document.getElementById("searchName").value;
  if (searchValue.length) {
    console.log(searchValue);
    searchByRank(searchValue);
  }

  if (searchValue === "") {
    showAllEmployee();
  }
});
