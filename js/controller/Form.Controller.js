class FormController {
  #_formFieldInstances = [];
  #_rules = [];
  #_errors = {};
  #_DEFAULT_ERROR_MESSAGE = {
    required: "Please do not leave this field empty",
    validEmail: "Pleasae enter a correct format email",
    minLength: "Please enter at least {param} character",
    maxLength: "Please enter maximum {param} character",
  };
  constructor(formSelector) {
    this.formElement = document.querySelector(formSelector);
    this.errors = [];
  }

  registerFields(formFields) {
    this.#_formFieldInstances = [...formFields];
  }
  setRules(rules) {
    this.#_rules = rules;
  }
  getRules() {
    return this.#_rules;
  }
  getFormFields() {
    return this.#_formFieldInstances;
  }
  setErrors(errors) {
    this.#_errors = errors;
  }
  getErrors() {
    return this.#_errors;
  }

  clearFormData() {
    this.setFormData();
  }

  setFormData(datalist = []) {
    let targetField;
    if (datalist.length) {
      this.#_formFieldInstances.forEach((formFieldInst, idx) => {
        //   set data
        targetField = this.formElement.querySelector(formFieldInst);
        let key = "";
        if (
          targetField.tagName === "INPUT" ||
          targetField.tagName === "SELECT"
        ) {
          key = "value";
        } else {
          key = "innerText";
        }
        targetField[key] = datalist[idx];
      });
    } else {
      this.#_formFieldInstances.forEach((formFieldInst) => {
        targetField = this.formElement.querySelector(formFieldInst);
        let key = "";
        if (
          targetField.tagName === "INPUT" ||
          targetField.tagName === "SELECT"
        ) {
          key = "value";
        } else {
          key = "innerText";
        }
        targetField[key] = "";
      });
    }
  }

  getFormData() {
    let targetField;
    let targetFieldVal = "";
    let returnedData = {};
    try {
      if (this.#_formFieldInstances.length) {
        this.#_formFieldInstances.forEach((formField) => {
          targetField = this.formElement.querySelector(formField);
          if (
            targetField.tagName === "INPUT" ||
            targetField.tagName === "SELECT"
          ) {
            targetFieldVal = targetField.value.trim();
          } else {
            targetFieldVal = targetField.innerText.trim();
          }
          returnedData[targetField.dataset.name] = targetFieldVal;
        });
      } else {
        throw "Please enter a list of formField";
      }
    } catch (err) {
      console.log("Error" + err);
    }
    return returnedData;
  }

  #processParamInText(txt, data) {
    const regex = /{([^}]*)}/;
    let param = txt.match(regex);
    if (param) {
      return txt.replace(param[0], data);
    }
    return txt;
  }

  setupController(ruleObj) {
    Object.entries(ruleObj).forEach(([k, v]) => {
      this.#_formFieldInstances.push(v.type === 1 ? "#" + k : "." + k);
    });
  }

  processRules(ruleObj, validatorObj) {
    const regex = /(.*?)\[(.*)\]/;
    let totalValidation = true;
    Object.entries(ruleObj).forEach(([k, v]) => {
      let fieldName = k;
      let { type, rule, errorEl, errors } = v;
      let field_rule_arr = rule.split("|");
      let convert_field_rule_arr = [];
      fieldName = type === 1 ? "#" + fieldName : "." + fieldName;
      validatorObj.setValue(this.formElement.querySelector(fieldName).value);
      // create rule arr with format {function_name, para_name};
      convert_field_rule_arr = field_rule_arr.reduce((arr, current) => {
        let p = current.match(regex);
        let f = "";
        let para = "";
        if (p) {
          f = p[1];
          para = p[2];
        } else {
          f = current;
          para = null;
        }
        arr.push({ f, para });
        if (!errors.hasOwnProperty(f)) {
          errors[f] = this.#_DEFAULT_ERROR_MESSAGE[f];
        }
        errors[f] = this.#processParamInText(errors[f], para);
        return arr;
      }, []);
      this.setRules(convert_field_rule_arr);
      this.setErrors(errors);
      totalValidation &= this.#executeRule(errorEl, validatorObj);
    });
    return totalValidation;
  }

  #executeRule(fieldError, validatorObj) {
    let ruleArr = this.getRules();
    let errors = this.getErrors();
    if (ruleArr.length > 1) {
      let valid = true;
      let errorEl = this.formElement.querySelector(fieldError);
      ruleArr.forEach((r) => {
        let f = r.f;
        let p = r.para;
        if (valid) {
          if (!validatorObj[f](p)) {
            valid = false;
            errorEl.style.display = "block";
            errorEl.innerText = errors[f];
          } else {
            valid = true;
            errorEl.innerText = "";
          }
        }
      });
      return valid;
    } else if (ruleArr.length === 1) {
      let f = ruleArr[0].f;
      let p = ruleArr[0].para;
      return validatorObj[f](p);
    } else {
      console.log("Co loi ve arr");
    }
  }
}

export { FormController };
