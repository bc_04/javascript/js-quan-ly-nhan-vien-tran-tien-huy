import { Employee } from "../model/employee.model.js";
import { RenderUtils } from "../utils/Render.utils.js";
import {
  delLocalStorage,
  loadLocalStorage,
  saveLocalStorage,
} from "../utils/StoreData.utils.js";
import { findItemByKey } from "../utils/System.utils.js";
import { FormController } from "./Form.Controller.js";
import { formRules } from "../utils/Form.utils.js";
import { Validator } from "../model/Validator.model.js";
const STORAGE_KEY = "employeeList";
let employeeList = [];
let renderObj = new RenderUtils();
renderObj.setTargetEl("#tableDanhSach");
let formObject = new FormController("#myModal .modal-body form");
let validatorObject = new Validator();
formObject.setupController(formRules);
let convertDataToObj = (employeeList) => {
  for (let i = 0; i < employeeList.length; i++) {
    let currentEmployee = employeeList[i];
    employeeList[i] = new Employee(
      currentEmployee._account,
      currentEmployee._name,
      currentEmployee._email,
      currentEmployee._pass,
      currentEmployee._workDay,
      currentEmployee._payMent,
      currentEmployee._role,
      currentEmployee._workHour
    );
  }
};

let addEmployee = () => {
  if (formObject.processRules(formRules, validatorObject)) {
    let employee = formObject.getFormData();
    let newEmployeeObj = new Employee(
      employee._account,
      employee._name,
      employee._email,
      employee._pass,
      employee._workDay,
      employee._payMent,
      employee._role,
      employee._workHour
    );
    employeeList.push(newEmployeeObj);
    saveLocalStorage(STORAGE_KEY, employeeList);
    showAllEmployee();
  }
};

let deleteEmployee = (_account) => {
  let findIndex = findItemByKey(employeeList, "_account", _account);
  if (findIndex > -1) {
    employeeList.splice(findIndex, 1);
    delLocalStorage(STORAGE_KEY);
    if (employeeList.length) {
      saveLocalStorage(STORAGE_KEY, employeeList);
      showAllEmployee();
    } else {
      renderObj.clearContent();
    }
  }
};

let editEmployee = (_account) => {
  let findIndex = findItemByKey(employeeList, "_account", _account);
  if (findIndex > -1) {
    formObject.setFormData(Object.values(employeeList[findIndex]));
    document.getElementById("btnThemNV").setAttribute("disabled", "");
    document.getElementById("btnCapNhat").removeAttribute("data-index");
    document.getElementById("btnCapNhat").setAttribute("data-index", findIndex);
  }
};

let updateEmployee = (index) => {
  if (formObject.processRules(formRules, validatorObject)) {
    let newEmployee = formObject.getFormData();
    formObject.clearFormData();
    document.getElementById("btnThemNV").removeAttribute("disabled");
    document.getElementById("btnCapNhat").removeAttribute("data-index");
    let newEmployeeObj = new Employee(
      newEmployee._account,
      newEmployee._name,
      newEmployee._email,
      newEmployee._pass,
      newEmployee._workDay,
      newEmployee._payMent,
      newEmployee._role,
      newEmployee._workHour
    );
    employeeList[index] = newEmployeeObj;
    delLocalStorage(STORAGE_KEY);
    saveLocalStorage(STORAGE_KEY, employeeList);
    showAllEmployee();
  }
};
let showAllEmployee = () => {
  employeeList = loadLocalStorage(STORAGE_KEY);
  if (employeeList.length) {
    convertDataToObj(employeeList);
    renderObj.setTargetData(employeeList);
    let editCallBack = editEmployee;
    let deleteCallBack = deleteEmployee;
    renderObj.renderHTML({ editCallBack, deleteCallBack });
  }
};

let searchByRank = (rank) => {
  let employeeWithRank = [];
  employeeList.forEach((employee) => {
    if (employee.rankEmployee() === rank) {
      employeeWithRank.push(employee);
    }
  });
  let editCallBack = editEmployee;
  let deleteCallBack = deleteEmployee;
  renderObj.setTargetData(employeeWithRank);
  renderObj.renderHTML({ editCallBack, deleteCallBack });
};
export {
  showAllEmployee,
  addEmployee,
  deleteEmployee,
  editEmployee,
  updateEmployee,
  searchByRank,
};
