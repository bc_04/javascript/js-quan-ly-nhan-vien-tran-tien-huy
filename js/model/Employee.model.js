class Employee {
  constructor(
    _account,
    _name,
    _email,
    _pass,
    _workDay,
    _payMent,
    _role,
    _workHour
  ) {
    this._account = _account;
    this._name = _name;
    this._email = _email;
    this._pass = _pass;
    this._workDay = _workDay;
    this._payMent = _payMent;
    this._role = _role;
    this._workHour = _workHour;
  }
  getRoleName() {
    switch (this._role) {
      case "1":
        return "Nhân viên";
      case "2":
        return "Trưởng phòng";
      case "3":
        return "Giám đốc";
      default:
        return undefined;
    }
  }
  rankEmployee() {
    let rank = "";
    if (this._workHour >= 192) {
      rank = "xuất sắc";
    } else if (this._workHour >= 176) {
      rank = "giỏi";
    } else if (this._workHour >= 160) {
      rank = "khá";
    } else {
      rank = "trung bình";
    }
    return rank;
  }
  calculateTotalPayment() {
    return this._payMent * parseInt(this._role);
  }
}

export { Employee };
