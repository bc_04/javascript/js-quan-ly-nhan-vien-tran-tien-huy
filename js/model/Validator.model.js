class Validator {
  #_value = "";
  constructor() {}

  setValue(value) {
    this._value = value;
  }
  getValue() {
    return this._value;
  }

  setErrors(errors) {
    this._errors = errors;
  }
  getErrors() {
    return this._errors;
  }
  required() {
    return this.getValue().trim().length > 0;
  }
  minLength(length) {
    return this.getValue().trim().length >= parseInt(length);
  }
  maxLength(length) {
    return this.getValue().trim().length <= parseInt(length);
  }
  greaterThan(val) {
    return parseInt(this.getValue().trim()) >= parseInt(val);
  }
  lessThan(val) {
    return parseInt(this.getValue().trim()) <= parseInt(val);
  }
  validEmail() {
    const EMAIL_REGEX =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return EMAIL_REGEX.test(this.getValue());
  }
  validNumbFormat() {
    const NUMB_REGEX = /^(?=.*\d)[\d ]+$/;
    return NUMB_REGEX.test(this.getValue());
  }

  validTextFormat() {
    const TEXT_NORMAL_REGEX = /^[a-zA-Z\s]*$/g;
    return TEXT_NORMAL_REGEX.test(this.getValue());
  }

  validPassFormat() {
    const TEXT_PASS_REGEX =
      /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^!&*+=_]).*$/gm;
    return TEXT_PASS_REGEX.test(this.getValue());
  }

  validDateFormat() {
    const DATE_REGEX =
      /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/gm;
    return DATE_REGEX.test(this.getValue());
  }

  inList(list) {
    return list.split(",").includes(this.getValue());
  }
}

export { Validator };
